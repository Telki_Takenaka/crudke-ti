<?php

namespace App\Http\Controllers;

use App\Models\Models\Product;
use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    public function index(Request $request){

        $query = product::query();

        $product_Id = $request-> product_Id;
        $name = $request->name;

        if (!empty($product_Id)) {
            $query->Where('product_Id', 'like', '%'. $product_Id.'%') ;
        }
        if (!empty($name)) {
            $query->orWhere('name', 'like', '%'. $name.'%') ;
        }

        $products = $query->orderBy('product_Id', 'desc')->paginate(10);
    	$pdf = PDF::loadView('output',[ 'products' => $products, 'request' => $request]);

        return $pdf->stream();

    }//
}
