<!doctype html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>登録商品一覧PDF</title>
  <style type="text/css">
  table {
      border-collapse: collapse;
    }

    tr {
      page-break-inside: avoid;
    }

    th,
    td {
      border: 1px solid black;
    }
    @font-face {
        font-family: ipag;
        font-style: normal;
        font-weight: normal;
        src: url('{{ storage_path('fonts/ipag.ttf') }}') format('truetype');
    }

    @font-face {
        font-family: ipag;
        font-style: bold;
        font-weight: bold;
        src: url('{{ storage_path('fonts/ipag.ttf') }}') format('truetype');
    }

    body {
        font-family: ipag !important;
    }
  </style>
</head>
<body>
  <h1>登録商品一覧</h1>
  <table>
    <tr>
      <th>商品ID</th>
      <th>商品名</th>
      <th>価格</th>
      <th>商品画像</th>
      <th>情報更新日</th>
      @foreach($products as $product) <tr>
        <td>{{ $product->product_Id }}</td>
        <td style="white-space: nowrap">{{$product->name}}</td>
        <td>{{$product->price}}円</td>
        <td><img src="{{$product->image}}" alt="{{$product->name}}"></td>
        <td>{{$product->product_Update_Time}}</td>
    </tr>
    @endforeach
  </table>
</body>
</html>