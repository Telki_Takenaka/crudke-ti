@extends('layouts.application')
@section('title', '新規商品追加確認')
@section('content')

<style>
    .form-horizontal{background-color: #eee; }
</style>

<br><br>
<form action="" method="post" class="form-horizontal">
    @csrf
    <input type="hidden" name="product_Id" value="{{$product_Id}}">
    <input type="hidden" name="name" value="{{$name}}">
    <input type="hidden" name="price" value="{{$price}}">
    <input type="hidden" name="image" value="{{$image}}">
    <script>
        document.write('<input type="hidden" name="product_Update_Time" value="' + (new Date).toLocaleString() + '">');
    </script>​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​​
    <div class="row">
        <label class="col-md-12 control-label">商品ID</label>
        <div class="col-md-12">{{$product_Id}}</div>
    </div>
    <div class="row">
        <label class="col-md-12 control-label">商品名</label>
        <div class="col-md-12">{{$name}}</div>
    </div>
    <div class="row">
        <label class="col-md-12 control-label">価格</label>
        <div class="col-md-12">{{$price}}円</div>
    </div>
    <div class="row">
        <label class="col-md-12 control-label">商品画像</label>
        <div class="col-md-12"><img src="{{$image}}"></div>
    </div>
    <div class="row">
        <label class="col-md-12 control-label">商品データ(確認用)</label>
        <div class="col-md-12">{{$image}}</div>
    </div>
    
    <div class="row" style="margin-top: 30px;">
        <div class="col-sm-offset-4 col-md-12">
            <input type="submit" value="登録" class="btn btn-primary btn-wide" />
        </div>
    </div>
</form>
<br><br>

@endsection