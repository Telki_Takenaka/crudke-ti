@extends('layouts.application')
@section('title', '新規商品追加')

<style>
    .form-group{background-color: #eee; }
</style>

@section('content')
<br><br>
<form action="" method="post" class="form-horizontal">
  @csrf
  {{ method_field('patch') }}
  @include('layouts.errors')
  <div class="form-group">
    <label for="product_Id" class="col-md-12 control-label">商品ID</label>
    <div class="col-md-12">
      <input type="text" class="form-control" id="product_Id" name="product_Id" value="{{old('product_Id')}}" placeholder="商品ID">
    </div>
  </div>
  <div class="form-group">
    <label for="name" class="col-md-12 control-label">商品名</label>
    <div class="col-md-12">
    <input type="text" class="form-control" name="name" value="{{old('name')}}" rows="8" placeholder="商品名">
    </div>
  <div class="form-group">
    <label for="price" class="col-md-12 control-label">価格</label>
    <div class="col-md-12">
    <input type="number" class="form-control" name="price" value="{{old('price')}}" rows="8" placeholder="価格">
    </div>
  </div>
  <div class="form-group">
    <label for="image" class="col-md-12 control-label">商品画像</label>
    <div class="col-md-12">
      <input type="file" class="form-control" name="image" value="{{old('image')}}" accept="image/jpeg,image/png" placeholder="商品画像" onchange="previewFile(this);">
      <input type="hidden" class="form-control" id="image_data" name="image" value="{{old('image')}}">
    </div>
  </div>
  <img src="" alt="商品画像のプレビュー">
  <script>
  //----------------------------------------------------------------------------------------------------------------
    function previewFile() {
      const preview = document.querySelector('img');
      const file = document.querySelector('input[type=file]').files[0];
      const reader = new FileReader();
      reader.addEventListener("load", function () {
        preview.src = reader.result;
        document.getElementById("image_data").value = preview.src;
      }, false);

      if (file) {
        reader.readAsDataURL(file);
      }
    }
  //----------------------------------------------------------------------------------------------------------------
  </script>
</head>
  <div class="text-right"><button class="btn btn-primary">確認</button></div>
</form>
<br><br>
@endsection