@extends('layouts.application')
@section('title', '更新')

@section('content')

<style>
    .form-horizontal{background-color: #eee; }
    .text-right{background-color: #eee; }
</style>

<br><br>
<form action="" method="post" class="form-horizontal">
  @csrf
  {{ method_field('patch') }}
  @include('layouts.errors')
  <div class="row">
      <label class="col-md-12 control-label">商品ID</label>
      <div class="col-md-12">{{$product->product_Id}}</div>
      <input type="text" class="form-control" name="product_Id" value="{{$product->product_Id}}">
  </div>
  <div class="form-group">
      <label class="col-md-12 control-label">商品名</label>
      <div class="col-md-12">{{$product->name}}</div>
      <input type="text" class="form-control" name="name" value="{{$product->name}}">
  </div>
  <div class="form-group">
      <label class="col-md-12 control-label">価格</label>
      <div class="col-md-12">{{$product->price}}円</div>
      <input type="number" class="form-control" name="price" value="{{$product->price}}">
  </div>
  <div class="form-group">
      <label class="col-md-12 control-label">商品画像</label>
      <div class="col-md-12"><img src="{{$product->image}}" alt="{{$product->name}}"></div>
      <input type="file" class="form-control"  name="image" value="{{$product->image}}" accept="image/jpeg,image/png" onchange="previewFile(this);">
      <input type="hidden" class="form-control" id="image_data" name="image" value="{{$product->image}}">
      <script>
      //----------------------------------------------------------------------------------------------------------------
        function previewFile() {
          const preview = document.querySelector('img');
          const file = document.querySelector('input[type=file]').files[0];
          const reader = new FileReader();
          reader.addEventListener("load", function () {
            preview.src = reader.result;
            document.getElementById("image_data").value = preview.src;
          }, false);

          if (file) {
            reader.readAsDataURL(file);
          }
        }
      //----------------------------------------------------------------------------------------------------------------
      </script>
  </div>
  </div>
  <div class="text-right"><button class="btn btn-primary">変更</button></div>
</form>
<br><br>
@endsection