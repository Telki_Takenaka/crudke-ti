@extends('layouts.application')
@section('title', '詳細')
@section('content')

<style>
    .row{background-color: #eee; }
</style>

<br>

<div class="row">
    <label class="col-md-12 control-label">商品ID</label>
    <div class="col-md-12">{{$product->product_Id}}</div>
</div>
<div class="form-group">
    <label class="col-md-12 control-label">商品名</label>
    <div class="col-md-12">{{$product->name}}</div>
</div>
<div class="form-group">
    <label class="col-md-12 control-label">価格</label>
    <div class="col-md-12">{{$product->price}}円</div>
</div>
<div class="form-group">
    <label class="col-md-12 control-label">商品画像</label>
    <div class="col-md-12"><img src="{{$product->image}}" alt="{{$product->name}}"></div>
</div>

<div class="row">
    <a href="{{ route('product.index') }}" class="btn btn-primary">戻る</a>
</div>
<br><br>

@endsection