@extends('layouts.application')
@section('title', '商品ページ')

<style>
    .form-group{background-color: #eee; }
    .table-condensed{background-color: #ccc; }
    .right_font{text-align: right;}
</style>

@section('script')
<script>
  function check(){
  if(window.confirm('本当に削除してもよろしいでしょうか？')){
    return true;
  }else{
    window.alert('キャンセルされました');
    return false;
  }
}
</script>
@endsection

@section('content')
<div class="col-sm-12" style="text-align:center;">

  <div class="form-inline row" style="padding-left:0px;">
    <!--↓↓ 検索フォーム ↓↓-->
    <div class="col-md-10">
      <form action="{{ route('product.index') }}">
        @csrf
        <div class="form-group">
          <input type="text" name="product_Id" value="{{ $request->product_Id }}" class="form-control" placeholder="商品ID">
          <input type="text" name="name" value="{{ $request->name }}" class="form-control" placeholder="商品名">
          <input type="submit" value="検索" class="btn btn-primary">
        </div>
      </form>
    </div>
    <!--↑↑ 検索フォーム ↑↑-->
    <div class="col-md-2">
      <div class="form-group">
        <a href="{{ route('product.create') }}" class="btn btn-primary">新規商品追加</a>
      </div>
    </div>
  </div>

  <table class="table table-condensed table-striped table-hover">
    <thead>
      <tr>
        <th>商品ID</th>
        <th>商品名</th>
        <th>価格</th>
        <th>商品画像</th>
        <th>情報更新日</th>
      </tr>
    </thead>
    <tbody>
      　@foreach($products as $product)
      <tr>
        <td>{{$product->product_Id}}</td>
        <td>{{$product->name}}</td>
        <td ><div class="right_font">{{$product->price}}円</div></td>
        <td><img src="{{$product->image}}" alt="{{$product->name}}"></td>
        <td>{{$product->product_Update_Time}}</td>
        <td size="5">
          <div style="display:flex;">
            <a href="{{ route('product.show', $product) }}" class="btn btn-primary btn-sm">詳細</a>&nbsp;
            <a href="{{ route('product.edit', $product) }}" class="btn btn-primary btn-sm">編集</a>&nbsp;
            <form action="{{ route('product.delete', $product) }}" method="post" onSubmit="return check()">
              @csrf
              <input type="submit" value="削除">
            </form>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

@endsection