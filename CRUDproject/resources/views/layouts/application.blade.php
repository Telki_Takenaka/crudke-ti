<!doctype html>
<html lang="ja">

<head>
  <meta charset="utf-8">
  <title>@yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/app.css">
</head>

<body>
  <div id="app"> 
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
      <div class="container">
        <!-- <a class="navbar-brand" href="{{ url('/topmenu') }}">
          Top
        </a> -->

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="pdf">
                PDF表示
              </a>
            </li>
          </ul>

          <ul class="navbar-nav">
          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
          </ul>
        </div>
      </div>
    </nav>
  </div>

  <div class="container">
    <div class="row" id="content">
      <!-- メインコンテンツ -->
      @yield('content')
    </div>
  </div>
  <script src="/js/app.js" defer></script>
  @yield('script')
</body>
</html>