<?php

use Illuminate\Support\Facades\Route;
use App\Models\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('pdf','App\Http\Controllers\PDFController@index');

Route::group(['prefix' => 'product'], function () {


    //一覧
    Route::get('/', 'App\Http\Controllers\ProductController@index')->name('product.index');


    //詳細
    Route::get('show/{product}/', function (product $product) {
        return view('product/show', ['product' => $product]);
    })->name('product.show');

    
    //新規作成
    Route::view('create', 'product/create/index')->name('product.create');
    Route::patch('create', 'App\Http\Controllers\ProductController@create')->name('product.create');
    Route::post('create', 'App\Http\Controllers\ProductController@store')->name('product.create');


    //編集
    Route::get('edit/{product}/', function (product $product) {
        return view('product/edit/index', [ 'product' => $product]);
    })->name('product.edit');
    Route::patch('edit/{product}/', 'App\Http\Controllers\ProductController@edit')->name('product.edit');
    Route::post('edit/{product}/', 'App\Http\Controllers\ProductController@update')->name('product.edit');


    //削除
    // Route::post('delete/{Product}/', 'ProductController@destroy')->name('product.delete');
    Route:: post('delete/{product}/', function (product $product) {
        $product->delete();
        return redirect()->to('product');
    })->name('product.delete');
});

?>